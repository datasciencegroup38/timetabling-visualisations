# Timetabling visualisations #
This repository holds except from the input data all files that are needed to visualize the timetabling data in Tableau.

## Requirements ##
- Tableau
- MySQLWorkbench
- Spoon (Pentaho Data Integration)

## How to use ##
1. Obtain the data containing all activity data from the project owner.
2. In MySQLWorkbench, create a new database.
3. Open the Timetables.mwb in MySQLWorkbench and Forward Engineer it to the database created in step 2.
4. Import the data of buildings.csv using MySQLWorkbench. To do this, right click on the Buildings table in MySQLWorkbench and choose Table Data Import Wizard.
5. Open the activities_transformation in Spoon. Configure Spoon to use a new MySQL connection. To do this, click on the "Table output" step and press new connection and follow the wizard. 
6. Configure the steps "CSV file input activities" and CSV file input activities "2", choose the file location and press Get fields. Then configure "Add CollegeYear" and "Add CollegeYear 2" to configure the data of the college year. 
7. Repeat step 6 for every input file. You will have one input file for every college year.
8. Run the configuration.
9. Open the Tableau project file and connect to you MySQL database. Make sure to relate the to tables in the database by the building ID.
10. Done. The visualisations should now be visibile.